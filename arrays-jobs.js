let employeeList = require('./employee')

// find job
function fidnAllWebDeveloper(employeeList, job) {
    let allWebDevloper = employeeList.filter((employee) => {
        return employee.job.includes(job)
    })

    return allWebDevloper;

}


// convert salary value string to number
function salaryValueIntoNumber(employeeList) {
    employeeList.map((employee) => {
        employee.salary = employee.salary.slice(1);
        employee.salary = Number(employee.salary)

        return employee

    })
    return employeeList
}




//currecting salary amount
function currectingSalary(employeeList) {
    employeeList = salaryValueIntoNumber(employeeList);

    employeeList.map((employee) => {
        employee["corrected_salary"] = (employee.salary * 10000).toFixed(2);
        return employee;
    })

    return employeeList;
}




// sum of all salary
function sumAllSalary(employeeList) {
    employeeList = salaryValueIntoNumber(employeeList);

    let sumSalary = employeeList.reduce((sum, employee) => {
        sum += employee.salary;
        return sum;
    }, 0)

    return sumSalary.toFixed(2)
}




// Find the sum of all salaries based on country using only HOF method
function sumSalaryBasedOnCountry(employeeList, country) {
    employeeList = salaryValueIntoNumber(employeeList);

    let sum = 0;
    let salarySum = employeeList.filter((employee) => {
        if (employee.location == country) {
            sum += employee.salary;
        }
        return sum
    })

    return sum.toFixed(2)
}



// Find the average salaries based on country using only HOF method
function sumSalaryBasedOnCountry(employeeList, country) {
    employeeList = salaryValueIntoNumber(employeeList);
    let num = 0
    let sum = 0;
    let avg = 0;
    employeeList.filter((employee) => {
        if (employee.location == country) {
            num = num + 1
            sum += employee.salary;
        }
        average = sum / num
        return average
    })

    return average.toFixed(2)
}

